<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\VoitureRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\RechercheVoiture;
use App\Form\RechercheVoitureType;
use App\Entity\Voiture;
use App\Form\VoitureType;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(VoitureRepository $repo ,
                         PaginatorInterface $paginatorInterface, 
                         Request $request)
    {
        $rechercheVoiture = new RechercheVoiture();
        $form = $this->createForm( RechercheVoitureType::class,  $rechercheVoiture);

        $form->handleRequest($request);
        
        $voitures =  $paginatorInterface->paginate(
            $repo->findAllWithPAgination($rechercheVoiture),
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );
        return $this->render('voiture/voitures.html.twig', [
            'voitures' => $voitures,
            'form' => $form->createView(),
            'admin' => true
        ]);
    }


    /**
     * @Route("/admin/creation", name="creationVoiture")
     * @Route("/admin/{id}", name="modifVoiture" , methods="GET|POST")
     */
    public function modificationEtAjout(Voiture $voiture=null, Request $request )
    {
        if ($voiture == null) {
            $voiture = new Voiture();
        }

        $manager = $this->getDoctrine()->getManager();
        $form = $this->createForm( VoitureType::class, $voiture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($voiture);
            $manager->flush();
            $this->addFlash('success' , "L'action a été effectuée");
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/modification.html.twig', [
            'voiture' => $voiture,
            'form' => $form->createView()
        ]);
    }


        /**
     * @Route("/admin/{id}", name="supVoiture" , methods="SUP")
     */
    public function supVoiture(Voiture $voiture, Request $request)
    {
       if ($this->isCsrfTokenValid("SUP".$voiture->getId(), $request->get("_token") )) {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($voiture);
            $manager->flush();
            $this->addFlash('success' , "L action a été effectuée");
            return $this->redirectToRoute("admin");
       }
    }


}
