<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class RechercheVoiture 
{
    /**
     * @Assert\LessThanOrEqual(propertyPath="maxAnnee" ,  message ="doit etre plus petit que l'annee max")
     */
    private $minAnnee;

    /**
     * @Assert\GreaterThanOrEqual(propertyPath="minAnnee" ,  message ="doit etre plus grand que l'annee min")
     */
    private $maxAnnee;    

    public function getMinAnnee()
    {
        return $this->minAnnee;
    }
    public function setMinAnnee(int  $minAnnee)
    {
        $this->minAnnee = $minAnnee;
        return $this;
    }

    public function getMaxAnnee()
    {
        return $this->maxAnnee;
    }
    public function setMaxAnnee(int $maxAnnee)
    {
        $this->maxAnnee = $maxAnnee;
        return $this;
    }
}
